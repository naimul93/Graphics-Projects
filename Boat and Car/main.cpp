#include<windows.h>
#include <GL/gl.h>
#include <GL/glut.h>
#include <stdlib.h>
#include <math.h>





static GLfloat x = 0;

static GLfloat b1 = 0;
static GLfloat b2 = -8;
static GLfloat c1 = -40;
static GLfloat c2 = -40;


void spinDisplay(void)

{

        b1 += 0.02;  //boat1
        if(b1 >= 72.0)//animation limit
		b1 = 0;

        b2 += 0.02;  //boat1
        if(b2 >= 72.0)//animation limit
		b2 = -8;
  c1+=0.02;//car1
    if(c1>=80)
        c1=-40;
  c2+=0.02;//car2
    if(c2>=80)
        c2=-40;



x=x+0.5;
	glutPostRedisplay();
}





void boat()//boat1
{
   glColor3f(0.0,0.0,0.0);//boat body
glBegin(GL_POLYGON);
  glVertex2f(-8,-50);
  glVertex2f(-10,-46);
   glVertex2f(-10,-41);
    glVertex2f(-8,-37);
    glVertex2f(-6,-41);
    glVertex2f(-6,-46);
     glEnd();

     glColor3f(0.6f,0.70f,0.05f);//boat roof
glBegin(GL_POLYGON);

   glVertex2f(-6,-45);
    glVertex2f(-10,-45);

    glVertex2f(-10,-42);
    glVertex2f(-9,-41);
    glVertex2f(-7,-41);
    glVertex2f(-6,-42);

     glEnd();

     glBegin(GL_TRIANGLES);
   glVertex2f(-6,-45);
    glVertex2f(-6,-46);
    glVertex2f(-7,-45);
     glEnd();

     glBegin(GL_TRIANGLES);
   glVertex2f(-9,-45);
    glVertex2f(-10,-45);
    glVertex2f(-10,-46);
     glEnd();


}
void boat2()//boat2
{
   glColor3f(0.0,0.0,0.0);//boat body
glBegin(GL_POLYGON);
  glVertex2f(8,-50);
  glVertex2f(10,-46);
   glVertex2f(10,-41);
    glVertex2f(8,-37);
    glVertex2f(6,-41);
    glVertex2f(6,-46);
     glEnd();

     glColor3f(0.5, 0.0, 0.0);//boat roof
glBegin(GL_POLYGON);
   glVertex2f(6,-45);
    glVertex2f(10,-45);

    glVertex2f(10,-42);
    glVertex2f(9,-41);
    glVertex2f(7,-41);
    glVertex2f(6,-42);
     glEnd();

          glBegin(GL_TRIANGLES);
   glVertex2f(6,-45);
    glVertex2f(6,-46);
    glVertex2f(7,-45);
     glEnd();

     glBegin(GL_TRIANGLES);
   glVertex2f(9,-45);
    glVertex2f(10,-45);
    glVertex2f(10,-46);
     glEnd();


}
void car()
{
    glColor3f(1.0f,0.0f,0.0f);

   glBegin(GL_POLYGON);
  glVertex2f(0,0);
  glVertex2f(0,2);
   glVertex2f(-1,2);
    glVertex2f(-3,5);
    glVertex2f(-8,5);
    glVertex2f(-9,2);
    glVertex2f(-11,1.3);
    glVertex2d(-11,0);

    glEnd();

glPushMatrix();
glColor3f(0,0,0);
glTranslatef(-3.5,0, 10);
glutSolidSphere(1,10,10);
glPopMatrix();

glPushMatrix();
glColor3f(0,0,0);
glTranslatef(-7,0, 10);
glutSolidSphere(1,10,10);
glPopMatrix();

       glColor3f(0.0f,0.0f,0.0f);//car window

   glBegin(GL_POLYGON);
  glVertex2f(-3,2);
  glVertex2f(-4,2);
   glVertex2f(-4,3.7);
    glVertex2f(-3,3.7);
    glEnd();

      glBegin(GL_POLYGON);
  glVertex2f(-5,2);
  glVertex2f(-6,2);
   glVertex2f(-6,3.7);
    glVertex2f(-5,3.7);
    glEnd();

     glBegin(GL_TRIANGLES);
  glVertex2f(-7,2);
  glVertex2f(-8,2);
    glVertex2f(-7,3.7);
    glEnd();
}
void car2()//car2
{
    glColor3f(0.2f,0.70f,0.5f);

   glBegin(GL_POLYGON);
 glVertex2f(0,0);
  glVertex2f(0,2);
   glVertex2f(1,2);
    glVertex2f(3,5);
    glVertex2f(8,5);
    glVertex2f(9,2);
    glVertex2f(11,1.3);
    glVertex2d(11,0);
    glEnd();

    glPushMatrix();
glColor3f(0,0,0);
glTranslatef(3.5,0, 10);
glutSolidSphere(1,10,10);
glPopMatrix();

glPushMatrix();
glColor3f(0,0,0);
glTranslatef(7,0, 10);
glutSolidSphere(1,10,10);
glPopMatrix();

       glColor3f(0.0f,0.0f,0.0f);//car2 window

   glBegin(GL_POLYGON);
  glVertex2f(3,2);
  glVertex2f(4,2);
   glVertex2f(4,3.7);
    glVertex2f(3,3.7);
    glEnd();

      glBegin(GL_POLYGON);
  glVertex2f(5,2);
  glVertex2f(6,2);
   glVertex2f(6,3.7);
    glVertex2f(5,3.7);
    glEnd();

     glBegin(GL_TRIANGLES);
  glVertex2f(7,2);
  glVertex2f(8,2);
    glVertex2f(7,3.7);
    glEnd();

}
void road()
{
      glColor3f(0.35f,0.35f,0.35f);//rood
glBegin(GL_QUADS);
  glVertex2f(-40,9);
  glVertex2f(40,9);
   glVertex2f(40,-10);
    glVertex2f(-40,-10);
     glEnd();

     glColor3f(1.0f,1.0f,1.0f);
  glBegin(GL_QUADS);
  glVertex2f(-40,1);
  glVertex2f(40,1);
  glVertex2f(40,-1);
  glVertex2f(-40,-1);
    glEnd();

    glColor3f(0.0f,0.0f,0.0f);
  glBegin(GL_QUADS);
  glVertex2f(-40,-10);
  glVertex2f(40,-10);
  glVertex2f(40,-12);
  glVertex2f(-40,-12);
    glEnd();


}

void display()
{
glClear(GL_COLOR_BUFFER_BIT);

glColor3f(0.6,0.6,0.9);//sky
glBegin(GL_POLYGON);
  glVertex2f(-40,50);
  glVertex2f(40,50);
  glColor3f(0.050f,0.52f,1.0f);
   glVertex2f(40,35);
    glVertex2f(-40,35);
    glEnd();





glColor3f(0.47f,0.73f,1.0f);//river
glBegin(GL_POLYGON);
glColor3f(0.4,0.9,1);
  glVertex2f(-20,35);
  glVertex2f(20,35);
   glColor3f(0.4,0.7,1);
    glVertex2f(30,-50);
    glVertex2f(-30,-50);
     glEnd();

     glColor3f(0.0f,0.0f,0.0f);//tree right 1
glBegin(GL_QUADS);
  glVertex2f(31,-24);
  glVertex2f(33,-24);
   glVertex2f(33,-28);
    glVertex2f(31,-28);
     glEnd();
     glColor3f(0.0f,0.43f,0.0f);
     glBegin(GL_TRIANGLES);
  glVertex2f(27,-24);
  glVertex2f(32,-19);
   glVertex2f(37,-24);

     glEnd();
     glColor3f(0.0f,0.43f,0.0f);
     glBegin(GL_TRIANGLES);
  glVertex2f(28,-20);
  glVertex2f(32,-16);
   glVertex2f(36,-20);

     glEnd();
glColor3f(0.0f,0.43f,0.0f);
     glBegin(GL_TRIANGLES);
  glVertex2f(29,-17);
  glVertex2f(32,-12);
   glVertex2f(35,-17);

     glEnd();



     glColor3f(0.0f,0.0f,0.0f);//tree left 1
glBegin(GL_QUADS);
  glVertex2f(-31,-24);
  glVertex2f(-33,-24);
   glVertex2f(-33,-28);
    glVertex2f(-31,-28);
     glEnd();
     glColor3f(0.0f,0.43f,0.0f);
     glBegin(GL_TRIANGLES);
  glVertex2f(-27,-24);
  glVertex2f(-32,-19);
   glVertex2f(-37,-24);

     glEnd();
     glColor3f(0.0f,0.43f,0.0f);
     glBegin(GL_TRIANGLES);
  glVertex2f(-28,-20);
  glVertex2f(-32,-16);
   glVertex2f(-36,-20);

     glEnd();
glColor3f(0.0f,0.43f,0.0f);
     glBegin(GL_TRIANGLES);
  glVertex2f(-29,-17);
  glVertex2f(-32,-12);
   glVertex2f(-35,-17);

     glEnd();


  glColor3f(0.0f,0.0f,0.0f);//tree right 2
glBegin(GL_QUADS);
  glVertex2f(27.5,30);
  glVertex2f(28.5,30);
    glVertex2f(28.5,33);
    glVertex2f(27.5,33);
     glEnd();

     glColor3f(0.0f,0.43f,0.0f);
     glBegin(GL_TRIANGLES);
  glVertex2f(25,33);
  glVertex2f(31,33);
   glVertex2f(28,38);

     glEnd();


 glColor3f(0.0f,0.0f,0.0f);//tree right 3
glBegin(GL_QUADS);
  glVertex2f(32.5,30);
  glVertex2f(33.5,30);
    glVertex2f(32.5,33);
    glVertex2f(33.5,33);
     glEnd();

     glColor3f(0.0f,0.43f,0.0f);
     glBegin(GL_TRIANGLES);
  glVertex2f(25,33);
  glVertex2f(31,33);
   glVertex2f(28,38);

     glEnd();


glPushMatrix();//sun


glPushMatrix();
glColor3f(1,1,0.2);
glTranslatef(30,45, 10);
glutSolidSphere(2, 50, 50);
glPopMatrix();


glutDisplayFunc(display);	glPopMatrix();



glPushMatrix();//boat1
	glTranslatef(-1,b1,0);
	boat();
	glPopMatrix();

glPushMatrix();//boat2
glTranslatef(1,b2,0);
	boat2();
	glPopMatrix();


road();

glPushMatrix();//car
	glTranslatef(-c1,5,0);
	car();
	glPopMatrix();

glPushMatrix();//car2
	glTranslatef(c2,-5,0);
	car2();
	glPopMatrix();


glutSwapBuffers();

}

void mouse(int button, int state, int x, int y)
{
   switch (button) {
      case GLUT_LEFT_BUTTON:
         if (state == GLUT_DOWN)
		 {

			 glutIdleFunc(spinDisplay);
		 }
			 break;
      case GLUT_MIDDLE_BUTTON:
      case GLUT_RIGHT_BUTTON:
         if (state == GLUT_DOWN)glutDisplayFunc(display);
		 {

			 glutIdleFunc(NULL);
		 }
         break;
      default:
         break;
   }
}


void init (void)
{
   glClearColor (0.0,0.76,0.0, 0.0);//Background
   glOrtho( -40, 40,-50, 50, -40,40);//Range of the co-ordinates
}


int main()
{
   glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
   glutInitWindowSize (800,1000);
   glutInitWindowPosition (0,0);
   glutCreateWindow("Green");
   init();
   glutIdleFunc(spinDisplay);
   glutDisplayFunc(display);
     glutMouseFunc(mouse);
   glutMainLoop();
   return 0;
}

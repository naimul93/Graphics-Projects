#include <windows.h>
#include <GL/glut.h>
void Draw()
{
 glClear(GL_COLOR_BUFFER_BIT);


 // Triangle_1
 glColor3f(0,0,0.8);
 glBegin(GL_TRIANGLES);
 glVertex2f(10.0,30.0);
 glVertex2f(10.0,10.0);
 glVertex2f(20.0,30.0);
 glEnd();

 // Triangle_2
 glColor3f(1,1,0);
 glBegin(GL_TRIANGLES);
 glVertex2f(10.0,10.0);
 glVertex2f(20.0,30.0);
 glVertex2f(30.0,30.0);
 glEnd();


 // Polygon
 glColor3f(1,0,0);
 glBegin(GL_POLYGON);
 glVertex2f(10.0,10.0);
 glVertex2f(30.0,30.0);
 glVertex2f(40.0,30.0);
 glVertex2f(40.0,23.0);
 glEnd();

// Triangle_3
 glColor3f(1,1,1);
 glBegin(GL_TRIANGLES);
 glVertex2f(10.0,10.0);
 glVertex2f(40.0,23.0);
 glVertex2f(40.0,17.0);
 glEnd();

 // Triangle_4
 glColor3f(0,0.6,0);
 glBegin(GL_TRIANGLES);
 glVertex2f(10.0,10.0);
 glVertex2f(40.0,17.0);
 glVertex2f(40.0,10.0);
 glEnd();

 glFlush();
}
void Initialize()
{glClearColor(0, 0, 0, 0);
 glMatrixMode(GL_PROJECTION);
 glLoadIdentity();
 glOrtho(0.0, 45.0, -45.0, 45.0, -45.0, 30.0);
}
int main(int Argc, char** Argv)
{
 glutInit(&Argc, Argv);
 glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
 glutInitWindowSize(500, 500);
 glutInitWindowPosition(200, 200);
 glutCreateWindow("Seychelles");
 Initialize();
 glutDisplayFunc(Draw);
 glutMainLoop();
 return 0;
}

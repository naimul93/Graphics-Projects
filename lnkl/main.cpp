#include <windows.h>
#include <GL/glut.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <time.h>
#define PI 3.1416
GLfloat s1 =0, s2 =0;
GLfloat b1=0, b2=0, b3=0, b4=0;



void drawDisk(double radius) {
	int d;
	glBegin(GL_POLYGON);
	for (d = 0; d < 32; d++) {
		double angle = 2*PI/32 * d;
		glVertex2d( radius*cos(angle), radius*sin(angle));
	}
	glEnd();
}

void drawCircle() {
	glColor3f(0,0,0);
	drawDisk(3.2);

}
void field()
{


    glColor3f (	0.627, 0.322, 0.176);
	glBegin(GL_POLYGON);
      glVertex3f (50, 40, 0);
      glVertex3f (0, 40, 0);
      glVertex3f (0, 0, 0);
      glVertex3f (50, 0, 0);
	glEnd();

}

void tree()
{
    glColor3f (0.196, 0.804, 0.196);//1
	glBegin(GL_POLYGON);
      glVertex3f (12, 28, 0);
      glVertex3f (10.5, 24.5, 0);
      glVertex3f (13.5, 24.5, 0);
	glEnd();



}

void tree1()
{
    glColor3f (0.804, 0.361, 0.361);



	glBegin(GL_POLYGON);
      glVertex3f (12.5,25, 0);
      glVertex3f (11.9,25, 0);
      glVertex3f (11.5,20, 0);
      glVertex3f (12.9,20, 0);
	glEnd();

}


void building()
{
    glColor3f (0.0, 1.0, 1.60);
	glBegin(GL_POLYGON);
      glVertex3f (10, 40, 0);
      glVertex3f (0, 40, 0);
      glVertex3f (0, 20, 0);
      glVertex3f (10, 20, 0);
	glEnd();

}

void window1()
{
    glColor3f (1.0, 1.0, 0.0);



	glBegin(GL_POLYGON);
      glVertex3f (4, 38, 0);
      glVertex3f (2, 38, 0);
      glVertex3f (2, 36, 0);
      glVertex3f (4, 36, 0);
	glEnd();

}

void window2()
{
    glColor3f (1.0, 1.0, 1.0);



	glBegin(GL_POLYGON);
      glVertex3f (19, 33, 0);
      glVertex3f (16, 33, 0);
      glVertex3f (16, 30, 0);
      glVertex3f (19, 30, 0);
	glEnd();

}


void window3()
{
    glColor3f (1.0, 1.0, 1.0);



	glBegin(GL_POLYGON);
      glVertex3f (24, 33, 0);
      glVertex3f (21, 33, 0);
      glVertex3f (21, 30, 0);
      glVertex3f (24, 30, 0);
	glEnd();

}


void window4()
{
    glColor3f (0.686, 0.933, 0.933);



	glBegin(GL_POLYGON);
      glVertex3f (37,35,0);
      glVertex3f (33,35, 0);
      glVertex3f (33, 32, 0);
      glVertex3f (37, 32, 0);
	glEnd();

}



void flag()
{
    glColor3f (0.000, 0.392, 0.000);



	glBegin(GL_POLYGON);
      glVertex3f (47.5, 30, 0);
      glVertex3f (45, 30, 0);
      glVertex3f (45, 28, 0);
      glVertex3f (47.5,28, 0);
	glEnd();


	glPushMatrix();
  glTranslatef(46.08,29.06,0);
  glScalef(0.12,0.12,0);
	glColor3f(1,0,0);
	drawDisk(3.2);
glPopMatrix();

}


void flagstand ()
{
    glColor3f (0.0, 0.0, 0.00);



	glBegin(GL_POLYGON);
      glVertex3f (44.9, 30.0, 0);
      glVertex3f (44.8, 30, 0);
      glVertex3f (44.8, 20, 0);
      glVertex3f (44.9, 20, 0);
	glEnd();

}


void bird1 ()
{
    glColor3f (0.0, 0.0, 0.00);



	glBegin(GL_POLYGON);
      glVertex3f (26, 38.8, 0);
      glVertex3f (24, 39.6, 0);
      glVertex3f (26, 37.8, 0);
	glEnd();
}

void bird2 ()
{
    glColor3f (0.0, 0.0, 0.00);



	glBegin(GL_POLYGON);
      glVertex3f (26, 37.8, 0);
      glVertex3f (28, 39.5, 0);
      glVertex3f (26, 38.8, 0);
	glEnd();
}
void bird3 ()
{
    glColor3f (0.0, 0.0, 0.00);



	glBegin(GL_POLYGON);
      glVertex3f (15, 39, 0);
      glVertex3f (17.5, 37.8, 0);
      glVertex3f (17.5, 38.5, 0);
	glEnd();
}

void bird4 ()
{
    glColor3f (0.0, 0.0, 0.00);



	glBegin(GL_POLYGON);
      glVertex3f (17.5, 37.8, 0);
      glVertex3f (17.5, 38.5, 0);
      glVertex3f (20, 39, 0);
	glEnd();
}



void build  ()
{
    glColor3f (0.0, 0.00, 0.10);



	glBegin(GL_POLYGON);
      glVertex3f (25, 35, 0);
      glVertex3f (15, 35, 0);
      glVertex3f (15, 20, 0);
      glVertex3f (25, 20, 0);
	glEnd();

}


void build2 ()
{
    glColor3f (0.0, 0.60, 1.00);



	glBegin(GL_POLYGON);
      glVertex3f (40, 37.5, 0);
      glVertex3f (30, 37.5, 0);
      glVertex3f (30, 20, 0);
      glVertex3f (40, 20, 0);
	glEnd();

}





void road()
{
    glColor3f (0.412, 0.412, 0.412);
	glBegin(GL_POLYGON);
      glVertex3f (50, 15, 0);
      glVertex3f (0, 15, 0);
      glVertex3f (0, 5, 0);
      glVertex3f (50, 5, 0);
	glEnd();

}

void devider ()
{
    glColor3f (1.000, 0.980, 0.980);



	glBegin(GL_POLYGON);
      glVertex3f (8.8, 10.4, 0);
      glVertex3f (2, 10.4, 0);
      glVertex3f (2, 9.3, 0);
      glVertex3f (8.8, 9.3, 0);
	glEnd();

}


void devider1  ()
{
    glColor3f (1.000, 0.980, 0.980);



	glBegin(GL_POLYGON);
      glVertex3f (18, 10.5, 0);
      glVertex3f (12, 10.5, 0);
      glVertex3f (12, 9.4, 0);
      glVertex3f (18, 9.4, 0);
	glEnd();

}


void devider2  ()
{
    glColor3f (1.000, 0.980, 0.980);



	glBegin(GL_POLYGON);
      glVertex3f (40, 10.5, 0);
      glVertex3f (33, 10.5, 0);
      glVertex3f (33, 9.4, 0);
      glVertex3f (40, 9.4, 0);
	glEnd();

}

void devider3  ()
{
    glColor3f (1.000, 0.980, 0.980);



	glBegin(GL_POLYGON);
      glVertex3f (50, 10.5, 0);
      glVertex3f (44, 10.5, 0);
      glVertex3f (44, 9.4, 0);
      glVertex3f (50, 9.4, 0);
	glEnd();

}


void zebra()
{
    glColor3f (1.000, 0.980, 0.980);
	glBegin(GL_POLYGON);
      glVertex3f (30, 5.5, 0);
      glVertex3f (25, 6.0, 0);
      glVertex3f (25, 5.2, 0);
      glVertex3f (30, 5.0, 0);
	glEnd();

}
void zebra2()
{
    glColor3f (1.000, 0.980, 0.980);
	glBegin(GL_POLYGON);
      glVertex3f (30, 6.2, 0);
      glVertex3f (25, 6.5, 0);
      glVertex3f (25, 7.3, 0);
      glVertex3f (30, 7.0, 0);
	glEnd();

}

void zebra3()
{
    glColor3f (1.000, 0.980, 0.980);
	glBegin(GL_POLYGON);
      glVertex3f (30, 8.5, 0);
      glVertex3f (25, 9.1, 0);
      glVertex3f (25, 8.3, 0);
      glVertex3f (30, 7.8, 0);
	glEnd();

}

void zebra4()
{
    glColor3f (1.000, 0.980, 0.980);
	glBegin(GL_POLYGON);
      glVertex3f (30, 9.8, 0);
      glVertex3f (25, 10.5, 0);
      glVertex3f (25, 9.7, 0);
      glVertex3f (30, 9.0, 0);
	glEnd();

}

void zebra5()
{
    glColor3f (1.000, 0.980, 0.980);
	glBegin(GL_POLYGON);
      glVertex3f (30, 11, 0);
      glVertex3f (25, 12, 0);
      glVertex3f (25, 11.2, 0);
      glVertex3f (30, 10.4, 0);
	glEnd();

}

void zebra6()
{
    glColor3f (1.000, 0.980, 0.980);
	glBegin(GL_POLYGON);
      glVertex3f (30, 11.5, 0);
      glVertex3f (25, 12.2, 0);
      glVertex3f (25, 12.0, 0);
      glVertex3f (30, 11.0, 0);
	glEnd();

}


void zebra7()
{
    glColor3f (1.000, 0.980, 0.980);
	glBegin(GL_POLYGON);
      glVertex3f (30, 10.9, 0);
      glVertex3f (25, 12.2, 0);
      glVertex3f (25, 11.8, 0);
      glVertex3f (30, 11.4, 0);
	glEnd();

}


void zebra8()
{
    glColor3f (1.000, 0.980, 0.980);
	glBegin(GL_POLYGON);
      glVertex3f (30, 13.0, 0);
      glVertex3f (25, 13.8, 0);
      glVertex3f (25, 13.0, 0);
      glVertex3f (30, 12.1, 0);
	glEnd();

}

void zebra9()
{
    glColor3f (1.000, 0.980, 0.980);
	glBegin(GL_POLYGON);
      glVertex3f (30, 14.1, 0);
      glVertex3f (25, 14.8, 0);
      glVertex3f (25, 14.1, 0);
      glVertex3f (30, 13.4, 0);
	glEnd();

}


void carr()
{

    glColor3f (1.000, 0.0, 0.0);
	glBegin(GL_POLYGON);
      glVertex3f (6,12,0);
      glVertex3f (2,12, 0);
      glVertex3f (2,10, 0);
      glVertex3f (6,10, 0);
	glEnd();


	glColor3f (0.0, 0.02, 1.002);
	glBegin(GL_POLYGON);
      glVertex3f (8,10,0);
      glVertex3f (.5,10, 0);
      glVertex3f (.5,7, 0);
      glVertex3f (8,7, 0);
	glEnd();



glPushMatrix();
  glTranslatef(6,6.6,0);
  glScalef(0.3,0.3,0);
	glColor3f(0,0,0);
	drawDisk(3.2);
glPopMatrix();

glPushMatrix();
  glTranslatef(2,6.6,0);
  glScalef(0.3,0.3,0);
	glColor3f(0,0,0);
	drawDisk(3.2);
glPopMatrix();

}


void man()
{

glColor3f (0.561, 0.737, 0.561);
	glBegin(GL_POLYGON);
      glVertex3f (28,18,0);
      glVertex3f (26.5,18, 0);
      glVertex3f (26.5,15.5, 0);
      glVertex3f (28,15.5, 0);
	glEnd();

glPushMatrix();
  glTranslatef(27.255,18.6,0);
  glScalef(0.25,0.26,0);
	glColor3f(1,1,1);
	drawDisk(3.2);
glPopMatrix();
glPushMatrix();
  glTranslatef(27.455,18.6,0);
  glScalef(0.04,0.04,0);
	glColor3f(0,0,0);
	drawDisk(3.2);
glPopMatrix();
glPushMatrix();
  glTranslatef(27.055,18.6,0);
  glScalef(0.04,0.04,0);
	glColor3f(0,0,0);
	drawDisk(3.2);
glPopMatrix();
glPushMatrix();
glTranslatef(27.3,18.2,0);
  glScalef(0.08,0.04,0);
	glColor3f(0,0,0);
	drawDisk(3.2);
glPopMatrix();

glColor3f (0.000,0.000,0.000);
	glBegin(GL_POLYGON);
      glVertex3f (26.5,18,0);
      glVertex3f (26.5,17.5,0);
      glVertex3f (25,17,0);
      glVertex3f (25,17.5,0);
	glEnd();
glColor3f (0.000,0.000,0.000);
	glBegin(GL_POLYGON);
      glVertex3f (28,18,0);
      glVertex3f (28,17.5,0);
      glVertex3f (29.5,17,0);
      glVertex3f (29.5,17.5,0);
	glEnd();

glColor3f (0.000,0.000,0.000);
	glBegin(GL_POLYGON);
      glVertex3f (26.5,15.5,0);
      glVertex3f (26.5,14,0);
      glVertex3f (27,14,0);
      glVertex3f (27,15.5,0);
	glEnd();

glColor3f (0.000,0.000,0.000);
	glBegin(GL_POLYGON);
      glVertex3f (27.5,15.5,0);
      glVertex3f (27.5,14,0);
      glVertex3f (28,14,0);
      glVertex3f (28,15.5,0);
	glEnd();

}



void display(void)
{
  glPushMatrix();
  glTranslatef(0,0,0);
  glScalef(1,1,0);
  field();
  glPopMatrix();


glPushMatrix();
  glTranslatef(0,0,0);
  glScalef(1,1,0);
  road();
  glPopMatrix();

glPushMatrix();
  glTranslatef(0,0,0);
  glScalef(1,1,0);
  building();
  glPopMatrix();



   glPushMatrix();
  glTranslatef(0,0,0);
  glScalef(1,1,0);
  build  ();
  glPopMatrix();




   glPushMatrix();
  glTranslatef(0,0,0);
  glScalef(1,1,0);
  build2  ();
  glPopMatrix();




   glPushMatrix();
  glTranslatef(0,0,0);
  glScalef(1,1,0);
  zebra  ();
  glPopMatrix();


      glPushMatrix();
  glTranslatef(0,0,0);
  glScalef(1,1,0);
  zebra2  ();
  glPopMatrix();


      glPushMatrix();
  glTranslatef(0,0,0);
  glScalef(1,1,0);
  zebra3();
  glPopMatrix();



   glPushMatrix();
  glTranslatef(0,0,0);
  glScalef(1,1,0);
  zebra4();
  glPopMatrix();



    glPushMatrix();
  glTranslatef(0,0,0);
  glScalef(1,1,0);
  zebra5();
  glPopMatrix();


   glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   zebra6();
   glPopMatrix();


glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   zebra7();
   glPopMatrix();


glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   zebra8();
   glPopMatrix();


   glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   zebra9();
   glPopMatrix();




   glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   devider();
   glPopMatrix();


    glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   devider1();
   glPopMatrix();



glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   devider2();
   glPopMatrix();


   glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   devider3();
   glPopMatrix();


 glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   window1();
   glPopMatrix();


glPushMatrix();
   glTranslatef(4.5,0,0);
   glScalef(1,1,0);
   window1();
   glPopMatrix();


   glPushMatrix();
   glTranslatef(0,-4.5,0);
   glScalef(1,1,0);
   window1();
   glPopMatrix();


   glPushMatrix();
   glTranslatef(4.5,-4.5,0);
   glScalef(1,1,0);
   window1();
   glPopMatrix();


      glPushMatrix();
   glTranslatef(0,-9,0);
   glScalef(1,1,0);
   window1();
   glPopMatrix();


   glPushMatrix();
   glTranslatef(4.5,-9,0);
   glScalef(1,1,0);
   window1();
   glPopMatrix();


    glPushMatrix();
   glTranslatef(4.5,-14,0);
   glScalef(1,1,0);
   window1();
   glPopMatrix();


 glPushMatrix();
  glColor3f(1.000, 0.271, 0.000);
   glTranslatef(0,-44,0);
   glScalef(1,1.8,0);
   window1();
   glPopMatrix();





   glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   window2();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   flag();
   glPopMatrix();


     glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   flagstand();
   glPopMatrix();



   glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   tree();
   glPopMatrix();

 glPushMatrix();
   glTranslatef(0,2,0);
   glScalef(1,1,0);
   tree();
   glPopMatrix();

 glPushMatrix();
   glTranslatef(15,2,0);
   glScalef(1,1,0);
   tree();
   glPopMatrix();

    glPushMatrix();
   glTranslatef(15,4,0);
   glScalef(1,1,0);
   tree();
   glPopMatrix();


     glPushMatrix();
   glTranslatef(30,3,0);
   glScalef(1,1,0);
   tree();
   glPopMatrix();

    glPushMatrix();
   glTranslatef(30,1,0);
   glScalef(1,1,0);
   tree();
   glPopMatrix();

    glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   tree1();
   glPopMatrix();

     glPushMatrix();
   glTranslatef(15,0,0);
   glScalef(1,1,0);
   tree1();
   glPopMatrix();

     glPushMatrix();
   glTranslatef(15,1,0);
   glScalef(1,1,0);
   tree1();
   glPopMatrix();

     glPushMatrix();
   glTranslatef(15,2,0);
   glScalef(1,1,0);
   tree1();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(29.75,2,0);
   glScalef(1,1,0);
   tree1();
   glPopMatrix();


   glPushMatrix();
   glTranslatef(29.75,1,0);
   glScalef(1,1,0);
   tree1();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(29.75,0,0);
   glScalef(1,1,0);
   tree1();
   glPopMatrix();


   glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   window3();
   glPopMatrix();

   glPushMatrix();
   glTranslatef(0,-6,0);
   glScalef(1,1,0);
   window3();
   glPopMatrix();



   glPushMatrix();
   glTranslatef(-5,-6,0);
   glScalef(1,1,0);
   window3();
   glPopMatrix();


   glPushMatrix();
   glTranslatef(0,0,0);
   glScalef(1,1,0);
   window4();
   glPopMatrix();



   glPushMatrix();
   glTranslatef(0,-5,0);
   glScalef(1,1,0);
   window4();
   glPopMatrix();



   glPushMatrix();
   glTranslatef(0,-10,0);
   glScalef(1,1,0);
   window4();
   glPopMatrix();



   glPushMatrix();
   glTranslatef(s1,0,0);
   glScalef(1,1,0);
   glTranslatef(0,0,0);
   carr();
   glutSwapBuffers();
   glPopMatrix();


   glPushMatrix();
   glTranslatef(0,s2,0);
   glScalef(1,1,0);
   glTranslatef(0,0,0);
   man();
   glPopMatrix();


    glPushMatrix();
   glTranslatef(b1,0,0);
   glScalef(1,1,0);
   glTranslatef(0,0,0);
   bird1();
   glutSwapBuffers();
   glPopMatrix();


    glPushMatrix();
   glTranslatef(b2,0,0);
   glScalef(1,1,0);
   glTranslatef(0,0,0);
   bird2();
   glutSwapBuffers();
   glPopMatrix();

    glPushMatrix();
   glTranslatef(b3,0,0);
   glScalef(1,1,0);
   glTranslatef(0,0,0);
   bird3();
   glutSwapBuffers();
   glPopMatrix();


    glPushMatrix();
   glTranslatef(b4,0,0);
   glScalef(1,1,0);
   glTranslatef(0,0,0);
   bird4();
   glutSwapBuffers();
   glPopMatrix();

   glFlush();

}

void spinDisplay(void)
{


		s1 += 0.00600;//speed
	if(s1 >= 50.0)
		s1 = 0.0;




	glutPostRedisplay();
}


void spinDisplay8(void)
{


		s2 -= 0.00455;//speed
	if(s2 >= 80.0)
		s2 = 32.0;

	glutPostRedisplay();
}

void spinDisplayB(void)
{


		b1 += 0.00600;//speed
	if(b1 >= 50.0)
		b1 = 0.0;

		b2 += 0.00600;//speed
	if(b2 >= 50.0)
		b2 = 0.0;

		b3 += 0.00500;//speed
	if(b3 >= 50.0)
		b3 = 5;

		b4 += 0.00500;//speed
	if(b4 >= 50.0)
		b4 = 5;

	glutPostRedisplay();
}
void mouse(int button, int state, int x, int y)
{
   switch (button)
   {
      case GLUT_LEFT_BUTTON:

         if (state == GLUT_DOWN)

			 glutIdleFunc(spinDisplay);

         break;

      case GLUT_MIDDLE_BUTTON:

      case GLUT_RIGHT_BUTTON:

         if (state == GLUT_DOWN)
			 glutIdleFunc(NULL);
         break;

      default:

         break;
   }

   glutIdleFunc(spinDisplay8);
}

void my_keyboard(unsigned char key, int x, int y)
{

	switch (key)
	{

        case 'r':
			 glutIdleFunc(spinDisplay8);

			break;




		case 's':
			 glutIdleFunc(NULL);
			 break;



	  default:

			break;
	}
}

void init(void)
{
    glClearColor (0.118, 0.565, 1.000, 0.0);
	glOrtho(0, 50.0, 0, 40.0, -2.0, 2.0);
}



int main()
{
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize (1396, 768);
	glutInitWindowPosition (0, 0);
	glutCreateWindow ("Zubaer");
	init();

    glutIdleFunc(spinDisplayB);
	glutDisplayFunc(display);

    glutMouseFunc(mouse);

    glutKeyboardFunc(my_keyboard);

	glutMainLoop();
	return 0;
}





#include <GL/gl.h>
#include <GL/glut.h>
#include<math.h>
#include<windows.h>
#define PI 3.1416



void rectangle( float r, float g, float b, float fx, float fy,float sx, float sy, float tx, float ty, float fox, float foy)
{
    glColor3f(r,g,b);
    glBegin(GL_POLYGON);
    glVertex2f(fx,fy);
    glVertex2f(sx,sy);
    glVertex2f(tx,ty);
    glVertex2f(fox,foy);
    glEnd();
}
void line( float r, float g, float b, float fx, float fy,float sx,float sy)
{
    glColor3f(r,g,b);
    glBegin(GL_LINES);
    glVertex2f(fx,fy);
    glVertex2f(sx,sy);
    glEnd();
}
void triangle( float r, float g, float b, float fx, float fy,float sx, float sy, float tx, float ty)
{
    glColor3f(r,g,b);
    glBegin(GL_TRIANGLES);
    glVertex2f(fx,fy);
    glVertex2f(sx,sy);
    glVertex2f(tx,ty);
    glEnd();
}
void circle(float r, float g, float b, float x, float y, float init, float last, float radx, float rady)
{
    glColor3f(r,g,b);
    glBegin(GL_POLYGON);
    for(float i=init;i<=last;i++)
    {
        float j=(i*PI)/180;
        glVertex2f(x+(radx*cos(j)),y+(rady*sin(j)));
    }
    glEnd();

}
void boat1()
{
    glPushMatrix();
    glTranslated(0,0.1,0);
    glScaled(0.9,0.9,0);
    rectangle(0.4,0.5,0.60,0.82,0.45,0.95,0.45,0.96,0.50,0.82,0.48);
    triangle(0,0.5,0.5,0.89,0.508,0.86,0.508,0.89,0.485);
    triangle(1,0,1,0.89,0.508,0.89,0.62,0.83,0.508);
    triangle(1,1,0,0.89,0.49,0.95,0.49,0.89,0.60);
    glPopMatrix();
}

void wood()
{
    glColor3f(0.3,0.3,0.1);
    glBegin(GL_TRIANGLES);
    glVertex2f(0.02,0.2);
    glVertex2f(0.065,0.2);
    glVertex2f(0.04,0.3);
    glEnd();
}

void nitolfake()
{
     glPushMatrix();
     glTranslated(0.018,-0.03,0);
     glScaled(0.8,1,0);
    rectangle(0.5,0.3,0.1,0.055,0.3,0.022,0.3,0.022,0.16,0.055,0.16);
    glPopMatrix();
    glPushMatrix();
    glTranslated(0,0,0);
    glScaled(1.2,0.8,0);
    wood();
    glTranslated(0,0.04,0);
    wood();
    glTranslated(0,0.04,0);
    wood();
     glTranslated(0,0.04,0);
    wood();
     glTranslated(0,0.04,0);
    wood();
    glPopMatrix();
}
void nitol()
{
    glPushMatrix();
    glTranslated(0.05,0,0);
    nitolfake();
    glPopMatrix();
    glPushMatrix();
    glTranslated(0.17,0.02,0);
    glScaled(0.6,0.6,0);
    nitolfake();
    glTranslated(0.34,0.02,0);
    nitolfake();
    glTranslated(0.34,0.015,0);
    nitolfake();
    glTranslated(0.34,0.02,0);
    nitolfake();
    glPopMatrix();
    glPushMatrix();
    glTranslated(0.24,-0.02,0);
    nitolfake();
    glPopMatrix();
    glPushMatrix();
    glTranslated(0.44,0.01,0);
    nitolfake();
    glPopMatrix();
    glPushMatrix();
    glTranslated(0.65,0.01,0);
    nitolfake();
    glPopMatrix();
    glPushMatrix();
    glTranslated(0.85,-0.01,0);
    nitolfake();
    glPopMatrix();
}
void nitolmain()
{
    glPushMatrix();
    glTranslated(0,-0.05,0);
    nitol();
    glPopMatrix();
}
void tree()
{
    glColor3f(0.7,0.6,0.0); ///tree
    glBegin(GL_POLYGON);
    glVertex2f(0.891,0.65);
    glVertex2f(0.894,0.65);
    glVertex2f(0.894,0.672);
    glVertex2f(0.891,0.672);
    glEnd();
glColor3f(0.0,0.6,0.0); ///leaf1
    glBegin(GL_TRIANGLES);
    glVertex2f(0.89,0.674);
    glVertex2f(0.893,0.70);
    glVertex2f(0.895,0.674);
    glEnd();
glColor3f(0.0,0.6,0.0); ///leaf2
    glBegin(GL_TRIANGLES);
    glVertex2f(0.896,0.678);
    glVertex2f(0.896,0.669);
    glVertex2f(0.907,0.674);
    glEnd();
glColor3f(0.0,0.6,0.0); ///leaf3
    glBegin(GL_TRIANGLES);
    glVertex2f(0.889,0.678);
    glVertex2f(0.889,0.669);
    glVertex2f(0.876,0.674);
    glEnd();

glColor3f(0.0,0.6,0.0); ///leaf4
    glBegin(GL_TRIANGLES);
    glVertex2f(0.895,0.685);
    glVertex2f(0.897,0.676);
    glVertex2f(0.906,0.693);
    glEnd();
glColor3f(0.0,0.6,0.0); ///leaf5
    glBegin(GL_TRIANGLES);
    glVertex2f(0.890,0.685);
    glVertex2f(0.888,0.676);
    glVertex2f(0.878,0.693);
    glEnd();

}
void treemain()
{
    tree();
    glPushMatrix();
    glTranslated(-0.03,0,0);
    tree();
    glTranslated(-0.03,0,0);
    tree();
    glPopMatrix();
    glPushMatrix();
    glTranslated(-0.4,0,0);
    tree();
    glTranslated(-0.03,0,0);
    tree();
    glTranslated(-0.03,0,0);
    tree();
    glPopMatrix();
    glPushMatrix();
    glTranslated(-0.24,0,0);
    tree();
    glTranslated(-0.03,0,0);
    tree();
    glTranslated(-0.03,0,0);
    tree();
    glPopMatrix();
    glPushMatrix();
    glTranslated(-0.71,0,0);
    tree();
    glTranslated(-0.03,0,0);
    tree();
    glTranslated(-0.03,0,0);
    tree();
    glTranslated(-0.03,0,0);
    tree();
    glPopMatrix();
}
void boat2()
{
    glPushMatrix();
    glTranslated(0,-0.02,0);
    rectangle(0.4,0.5,0.60,0.22,0.45,0.35,0.45,0.366,0.50,0.22,0.48);
    triangle(0.67,0.7,0.2,0.29,0.508,0.29,0.62,0.23,0.508);
    triangle(0,0.5,0.5,0.29,0.508,0.26,0.508,0.29,0.485);
    triangle(1,0.58,0,0.29,0.49,0.35,0.49,0.29,0.60);
    glPopMatrix();

}
void parachuteone()
{
    /* parachute-1 */
    glPushMatrix();
    glTranslated(-0.2,0,0);
    circle(0,0,0,0.4,0.815,1,300,0.002,0.004);
    rectangle(0.9,0.2,0.4,0.40255,0.815,0.399,0.815,0.399,0.797,0.40255,0.796);
    rectangle(0.3,0.5,.8,0.4005,0.798,0.399,0.798,0.396,0.787,0.398,0.786);
    rectangle(0.3,0.5,.8,0.4025,0.798,0.40,0.798,0.405,0.787,0.408,0.786);
    rectangle(.9,.2,.4,0.409,0.812,0.393,0.812,0.393,0.809,0.409,0.809);
    circle(0.6,0.5,0.8,0.4,0.88,1,200,0.05,0.04);
    line(1,1,1,0.352,0.87,0.4,0.8);
    line(1,1,1,0.450,0.88,0.4,0.8);
    line(1,1,1,0.41,0.8752,0.4,0.8);
    line(1,1,1,0.38,0.873,0.4,0.8);
    glPopMatrix();
}

void parachutetwo()
{
    /* parachute-2 */
    circle(0.6,0.2,0.4,0.7,0.93,1,200,0.04,0.03);
    line(1,1,1,0.664,0.92,0.7,0.86);
    line(1,1,1,0.74,0.932,0.7,0.86);
    line(1,1,1,0.71,0.9255,0.7,0.86);
    line(1,1,1,0.68,0.9255,0.7,0.86);
    rectangle(0.7,0.5,.2,0.708,0.875,0.693,0.875,0.693,0.857,0.708,0.857);
    rectangle(0,0.0,0,0.705,0.872,0.696,0.872,0.696,0.857,0.705,0.857);
    circle(0.3,0,0,0.696,0.879,1,300,0.002,0.004);
    circle(0.3,0,0,0.6999,0.879,1,300,0.002,0.004);
    circle(0.3,0,0,0.705,0.879,1,300,0.002,0.004);
}
void parachutetwomain()
{
    glPushMatrix();
    glTranslated(0.05,-0.05,0);
    parachutetwo();
    glPopMatrix();
}
void sun()
{
    /* sun */
    circle(1,0.85,0.2,0.92,0.92,1,500,0.022,0.04);

}
void moon()
{
    /* moon */
    glPushMatrix();
    glTranslatef(0,-0.9,0);
    circle(1,1,1,0.92,0.92,1,500,0.022,0.04);
    glPopMatrix();
}
void init()
{
    /*screen color setup */
    glClearColor(0.0,0.0,0.0,0.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glOrtho(0,1,0,1,0,1);
}

float p_animx=0;
float p_animy=0;
float p2_animx=0;
float p2_animy=0;
float s_animx=0;
float s_animy=0;
float m_animx=0;
float m_animy=0;
float b_animx=0;
float b_animy=0;
float b2_animx=0;
float b2_animy=0;

void display()
{


/* sky */

    if(s_animy<-0.32)
    {
         rectangle(0,0,0.15,1,1,0,1,0,0.65,1,0.65);

    }
    else
        {
             rectangle(0.95,0.6,0.2,1,1,0,1,0,0.65,1,0.65);
        }

/* moon */
    glPushMatrix();
    glTranslated(m_animx,m_animy,0);
    moon();
    glPopMatrix();
     if(m_animx>-0.87)
    {
         m_animx=m_animx-0.00028;
    }
    if(m_animy<=0.88)
    {
         m_animy=m_animy+0.00015;
    }



/* sun */
    glPushMatrix();
    glTranslated(s_animx,s_animy,0);
    sun();
    glPopMatrix();
    if(s_animx>=-0.11)
    {
         s_animx=s_animx-0.00003;
    }
    if(s_animy>=-0.35)
    {
         s_animy=s_animy-0.00008;
    }



/* river */
    if(s_animy<-0.32)
    {
         rectangle(0,0.1,0.2,1,0.65,0,.65,0,.33,1,.33);

    }
    else
        {
             rectangle(0.4,0.8,1,1,0.65,0,.65,0,.33,1,.33);
        }

    treemain();
/* sand */
if(s_animy<-0.32)
    {
         rectangle(0.1,0.1,0.1,1,0.33,0,0.33,0,0,1,0);

    }
    else
        {
             rectangle(0.9,0.8,0.5,1,0.33,0,0.33,0,0,1,0);
        }

    glColor3f(0.0,0.4,0.5); //building 1
    glBegin(GL_POLYGON);
    glVertex2f(0.007,0.65);
    glVertex2f(0.022,0.65);
    glVertex2f(0.022,0.67);
    glVertex2f(0.007,0.68);
    glEnd();

 glColor3f(0.0,0.4,0.5); //building 2
    glBegin(GL_POLYGON);
    glVertex2f(0.025,0.65);
    glVertex2f(0.043,0.65);
    glVertex2f(0.043,0.69);
    glVertex2f(0.025,0.68);
    glEnd();

glColor3f(0.0,0.4,0.5); ///building 3
    glBegin(GL_POLYGON);
    glVertex2f(0.046,0.65);
    glVertex2f(0.061,0.65);
    glVertex2f(0.061,0.67);
    glVertex2f(0.046,0.68);
    glEnd();
glColor3f(0.0,0.4,0.5); ///building 4
    glBegin(GL_POLYGON);
    glVertex2f(0.211,0.65);
    glVertex2f(0.222,0.65);
    glVertex2f(0.222,0.666);
    glVertex2f(0.211,0.666);
    glEnd();

glColor3f(0.0,0.4,0.5); ///building 5
    glBegin(GL_POLYGON);
    glVertex2f(0.223,0.65);
    glVertex2f(0.233,0.65);
    glVertex2f(0.233,0.67);
    glVertex2f(0.223,0.677);
    glEnd();
glColor3f(0.0,0.4,0.5); ///building 6
    glBegin(GL_POLYGON);
    glVertex2f(0.235,0.65);
    glVertex2f(0.247,0.65);
    glVertex2f(0.247,0.665);
    glVertex2f(0.235,0.665);
    glEnd();
glColor3f(0.0,0.4,0.5); ///building 7
    glBegin(GL_POLYGON);
    glVertex2f(0.290,0.65);
    glVertex2f(0.299,0.65);
    glVertex2f(0.299,0.666);
    glVertex2f(0.290,0.666);
    glEnd();
glColor3f(0.0,0.4,0.5); ///building 8
    glBegin(GL_POLYGON);
    glVertex2f(0.30,0.65);
    glVertex2f(0.310,0.65);
    glVertex2f(0.310,0.68);
    glVertex2f(0.30,0.67);
    glEnd();
 glColor3f(0.0,0.4,0.5); ///building 9
    glBegin(GL_POLYGON);
    glVertex2f(0.311,0.65);
    glVertex2f(0.321,0.65);
    glVertex2f(0.321,0.666);
    glVertex2f(0.311,0.666);
    glEnd();
 glColor3f(0.0,0.4,0.5); ///building 10
    glBegin(GL_POLYGON);
    glVertex2f(0.36,0.65);
    glVertex2f(0.37,0.65);
    glVertex2f(0.37,0.665);
    glVertex2f(0.36,0.665);
    glEnd();
glColor3f(0.0,0.4,0.5); ///building 11
    glBegin(GL_POLYGON);
    glVertex2f(0.372,0.65);
    glVertex2f(0.380,0.65);
    glVertex2f(0.380,0.676);
    glVertex2f(0.372,0.666);
    glEnd();
glColor3f(0.0,0.4,0.5); ///building 12
    glBegin(GL_POLYGON);
    glVertex2f(0.382,0.65);
    glVertex2f(0.39,0.65);
    glVertex2f(0.39,0.665);
    glVertex2f(0.382,0.665);
    glEnd();
glColor3f(0.0,0.4,0.5); ///building 13
    glBegin(GL_POLYGON);
    glVertex2f(0.53,0.65);
    glVertex2f(0.54,0.65);
    glVertex2f(0.54,0.662);
    glVertex2f(0.53,0.662);
    glEnd();
glColor3f(0.0,0.4,0.5); ///building 14
    glBegin(GL_POLYGON);
    glVertex2f(0.545,0.65);
    glVertex2f(0.556,0.65);
    glVertex2f(0.556,0.662);
    glVertex2f(0.545,0.662);
    glEnd();
glColor3f(0.0,0.4,0.5); ///building 15
    glBegin(GL_POLYGON);
    glVertex2f(0.68,0.65);
    glVertex2f(0.689,0.65);
    glVertex2f(0.689,0.662);
    glVertex2f(0.68,0.662);
    glEnd();
glColor3f(0.0,0.4,0.5); ///building 16
    glBegin(GL_POLYGON);
    glVertex2f(0.69,0.65);
    glVertex2f(0.699,0.65);
    glVertex2f(0.699,0.678);
    glVertex2f(0.69,0.67);
    glEnd();
glColor3f(0.0,0.4,0.5); ///building 17
    glBegin(GL_POLYGON);
    glVertex2f(0.7,0.65);
    glVertex2f(0.71,0.65);
    glVertex2f(0.71,0.662);
    glVertex2f(0.7,0.662);
    glEnd();
glColor3f(0.0,0.4,0.5); ///building 18
    glBegin(GL_POLYGON);
    glVertex2f(0.76,0.65);
    glVertex2f(0.772,0.65);
    glVertex2f(0.772,0.666);
    glVertex2f(0.76,0.666);
    glEnd();
glColor3f(0.0,0.4,0.5); ///building 19
    glBegin(GL_POLYGON);
    glVertex2f(0.774,0.65);
    glVertex2f(0.786,0.65);
    glVertex2f(0.786,0.68);
    glVertex2f(0.774,0.67);
    glEnd();
glColor3f(0.0,0.4,0.5); ///building 20
    glBegin(GL_POLYGON);
    glVertex2f(0.788,0.65);
    glVertex2f(0.798,0.65);
    glVertex2f(0.798,0.666);
    glVertex2f(0.788,0.666);
    glEnd();

glColor3f(0.0,0.4,0.5); ///building 21
    glBegin(GL_POLYGON);
    glVertex2f(0.935,0.65);
    glVertex2f(0.946,0.65);
    glVertex2f(0.946,0.67);
    glVertex2f(0.935,0.68);
    glEnd();

glColor3f(0.0,0.4,0.5); ///building 22
    glBegin(GL_POLYGON);
    glVertex2f(0.947,0.65);
    glVertex2f(0.965,0.65);
    glVertex2f(0.965,0.69);
    glVertex2f(0.947,0.68);
    glEnd();
glColor3f(0.0,0.4,0.5); ///building 23
    glBegin(GL_POLYGON);
    glVertex2f(0.967,0.65);
    glVertex2f(0.981,0.65);
    glVertex2f(0.981,0.68);
    glVertex2f(0.967,0.67);
    glEnd();

/*  Boat-1 */
    glPushMatrix();
    glTranslated(b2_animx,b2_animy,0);
    boat1();
    glPopMatrix();
    if(b2_animx>=-0.7)
    {
         b2_animx=b2_animx-0.00005;
    }


/* Boat-2 */
    glPushMatrix();
    glTranslated(b_animx,b_animy,0);
    boat2();
    glPopMatrix();
    if(b_animx<=0.6)
    {
         b_animx=b_animx+0.00005;
    }


/* parachute-2 */
    glPushMatrix();
    glTranslated(p2_animx,p2_animy,0);
    parachutetwomain();
    glPopMatrix();
    if(p2_animx>=-0.6)
    {
         p2_animx=p2_animx-0.00002;
    }
    if(p2_animy<=0.06)
    {
         p2_animy=p2_animy+0.000009;
    }

    /* parachute-1 */
    glPushMatrix();
    glTranslated(p_animx,p_animy,0);
    parachuteone();
    glPopMatrix();
    if(p_animx<=0.8)
    {
         p_animx=p_animx+0.000019;
    }
    if(p_animy>=-0.04)
    {
         p_animy=p_animy-0.0000039;
    }
    nitolmain();

    glutPostRedisplay();
    glFlush();

}


int main ( int argc, char * argv[] )
{

    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(2000,1000);
    glutInitWindowPosition(0,0);
    glutCreateWindow("Sea Beach");
    init();
    glutDisplayFunc(display);
    glutMainLoop();
}
